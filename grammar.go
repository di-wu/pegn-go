package pegn

type Grammar interface {
	Meta
	Components
	PEGN() string // returns original grammar.pegn content
}

type Components interface {
	Nodes
	Checks
	Classes
	Tokens
}

type Checks interface {
	Checks() map[string]Check
}

type Nodes interface {
	NodeNames() []string
	NodeName(typ int) string
	NodeInt(name string) int
}

type Meta interface {
	Language
	Version
	Legal
	Home() string
}

type Legal interface {
	Copyright() string
	License() string
}

type Version interface {
	MajorVer() int
	MinorVer() int
	PatchVer() int
	PreVer() string
}

type Language interface {
	Lang() string
	LangExt() string
}

type Classes interface {
	Classes() map[string]Class
}

type Tokens interface {
	Tokens() map[string]Token
}

type Component interface {
	Ident() string
	Desc() string
	PEGN() string
}

type NodeParser interface {
	Component
	Parse(p *Parser) (*Node, error)
}

type Check interface {
	Component
	Check(p *Parser) (*Mark, error)
}

type Class interface {
	Component
	Check(r rune) bool
}

type Token interface {
	Component
	Value() string
}
