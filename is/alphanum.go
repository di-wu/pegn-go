package is

var Alphanum = alphanum{}

type alphanum struct{}

func (alphanum) Ident() string { return "alphanum" }
func (alphanum) PEGN() string  { return "[A-Z] / [a-z] / [0-9]" }
func (alphanum) Desc() string {
	return `upper or lower letter or digit 0 through 9 (no underscore)`
}
func (alphanum) Check(r rune) bool {
	return Alpha.Check(r) || Digit.Check(r)
}
