package is

var Upper = upper{}

type upper struct{}

func (upper) Ident() string     { return "upper" }
func (upper) PEGN() string      { return "[A-Z]" }
func (upper) Desc() string      { return `any ASCII uppercase letter` }
func (upper) Check(r rune) bool { return 'A' <= r && r <= 'Z' }
