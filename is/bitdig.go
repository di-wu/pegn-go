package is

var Bitdig = bitdig{}

type bitdig struct{}

func (bitdig) Ident() string     { return `bitdig` }
func (bitdig) PEGN() string      { return `[0-1]` }
func (bitdig) Desc() string      { return `digit 0 or 1` }
func (bitdig) Check(r rune) bool { return r == '0' || r == '1' }
