package is

var Octdig = octdig{}

type octdig struct{}

func (octdig) Ident() string { return `octdig` }
func (octdig) PEGN() string  { return `[0-7]` }
func (octdig) Desc() string  { return `digit between 0 and 7` }
func (octdig) Check(r rune) bool {
	return '0' <= r && r <= '7'
}
