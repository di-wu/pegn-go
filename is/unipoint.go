package is

var Unipoint = unipoint{}

type unipoint struct{}

func (unipoint) Ident() string { return "unipoint" }
func (unipoint) PEGN() string  { return "[u0000-u10FFFF]" }
func (unipoint) Desc() string {
	return `any valid UNICODE code point`
}

func (unipoint) Check(r rune) bool {
	return '\u0000' <= r && r <= '\U0010FFFF'
}
