package is

var Any = any{}

type any struct{}

func (any) Ident() string { return "any" }
func (any) PEGN() string  { return "[b0-b11111111]" }
func (any) Desc() string {
	return `any byte (per PEG original specification)`
}

func (any) Check(r rune) bool { return r > 0 }
