package is

var Punct = punct{}

type punct struct{}

func (punct) Ident() string { return `punct` }
func (punct) PEGN() string  { return `[x21-x2F] / [x3A-x40] / [x5B-x60] / [x7B-x7E]` }
func (punct) Desc() string {
	return `ASCII punctuation character`
}
func (punct) Check(r rune) bool {
	switch {
	case
		'\u0021' <= r && r <= '\u002F',
		'\u003A' <= r && r <= '\u0040',
		'\u005B' <= r && r <= '\u0060',
		'\u007B' <= r && r <= '\u007E':
		return true
	default:
		return false
	}
}
