package is

var Lowerhex = lowerhex{}

type lowerhex struct{}

func (lowerhex) Ident() string { return "lowerhex" }
func (lowerhex) PEGN() string  { return "[0-9] / [a-f]" }
func (lowerhex) Desc() string {
	return `digit 0 through 0 or lowercase letter a through f`
}
func (lowerhex) Check(r rune) bool {
	return Digit.Check(r) || 'a' <= r && r <= 'f'
}
