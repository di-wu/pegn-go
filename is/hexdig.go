package is

var Hexdig = hexdig{}

type hexdig struct{}

func (hexdig) Ident() string { return `hexdig` }
func (hexdig) PEGN() string  { return `[0-9] / [a-f] / [A-F]` }
func (hexdig) Desc() string  { return `digit 0 through 9 or letter a-f (lower or upper case)` }
func (hexdig) Check(r rune) bool {
	return Digit.Check(r) || ('a' <= r && r <= 'f') || ('A' <= r && r <= 'F')
}
