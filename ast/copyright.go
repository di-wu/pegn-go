package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Copyright <-- '# Copyright ' Comment EndLine
func Copyright(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Copyright, nd.NodeTypes)

	var err error
	var n *pegn.Node
	b := p.Mark()

	// '# Copyright '
	_, err = p.Expect("# Copyright ")
	if err != nil {
		p.Goto(b)
		return expected("'# Copyright '", node, p)
	}

	// Comment
	n, err = Comment(p)
	if err != nil {
		p.Goto(b)
		return expected("Comment", node, p)
	}
	node.AppendChild(n)

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		p.Goto(b)
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
