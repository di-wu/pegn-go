package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// TokenDef   <-- TokenId SP+ '<-' SP+
//                TokenVal (Spacing TokenVal)*
//                ComEndLine
func TokenDef(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.TokenDef, nd.NodeTypes)

	var err error
	var n *pegn.Node

	beg := p.Mark()

	n, err = TokenId(p)
	if err != nil {
		p.Goto(beg)
		return expected("TokenId", node, p)
	}
	node.AppendChild(n)

	// SP+ '<-' SP+
	_, err = p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1})
	if err != nil {
		p.Goto(beg)
		return nil, err
	}

	n, err = TokenVal(p)
	if err != nil {
		p.Goto(beg)
		return expected("TokenVal", node, p)
	}
	node.AdoptFrom(n)

	for {
		beg = p.Mark()

		n, err = Spacing(p)
		if err != nil {
			p.Goto(beg)
			break
		}
		node.AdoptFrom(n)

		n, err = TokenVal(p)
		if err != nil {
			p.Goto(beg)
			return expected("TokenVal", node, p)
		}
		node.AdoptFrom(n)

	}

	// ComEndLine
	n, err = ComEndLine(p)
	if err != nil {
		p.Goto(beg)
		return nil, err
	}
	node.AdoptFrom(n)

	return node, nil
}
