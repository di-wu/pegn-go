package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// BinRange <-- '[' Bin '-' Bin ']'
func BinRange(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.BinRange, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	_, err = p.Expect('[')
	if err != nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Binary
	n, err = Binary(p)
	if err != nil {
		p.Goto(beg)
		return expected("Binary", node, p)
	}
	node.AppendChild(n)

	// '-'
	_, err = p.Expect('-')
	if err != nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Binary
	n, err = Binary(p)
	if err != nil {
		p.Goto(beg)
		return expected("Binary", node, p)
	}
	node.AppendChild(n)

	// ']'
	_, err = p.Expect(']')
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
