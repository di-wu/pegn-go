package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// CheckId <-- (upper lower+)+
func CheckId(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.CheckId, nd.NodeTypes)

	var m *pegn.Mark

	// (upper lower+)+
	m, err := p.Check(is.Min{is.Seq{is.Upper, is.Min{is.Lower, 1}}, 1})
	if err != nil {
		return expected("(upper lower+)+", node, p)
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
