package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// AlphaRange <-- '[' Alpha '-' Alpha ']'
func AlphaRange(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.AlphaRange, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	_, err = p.Expect('[')
	if err != nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Letter
	n, err = Letter(p)
	if err != nil {
		p.Goto(beg)
		return expected("Letter", node, p)
	}
	node.AppendChild(n)

	// '-'
	_, err = p.Expect('-')
	if err != nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Letter
	n, err = Letter(p)
	if err != nil {
		p.Goto(beg)
		return expected("Letter", node, p)
	}
	node.AppendChild(n)

	// ']'
	_, err = p.Expect(']')
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
