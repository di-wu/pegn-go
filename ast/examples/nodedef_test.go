package ast_test

import (
	
	"gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleNodeDef() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Node <-- SP")
	n, _ = ast.NodeDef(p)
	n.Print()
	// Output:
	// ["NodeDef", [
	//   ["CheckId", "Node"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["TokenId", [
	//           ["ResTokenId", "SP"]
	//         ]]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}
