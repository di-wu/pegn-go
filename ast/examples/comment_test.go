package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleComment_none() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("\n")
	n, _ = ast.Comment(p)
	n.Print()
	// Output:
	// <nil>
}

func ExampleComment_space() {
	var n *pegn.Node
	p := new(pegn.Parser)
	// SP
	p.Init(" ")
	n, _ = ast.Comment(p)
	n.Print()
	// Output:
	// ["Comment", " "]
}

func ExampleComment_simple() {
	var n *pegn.Node
	p := new(pegn.Parser)
	// Comment 👌 here.
	str := "Comment 👌 here."
	p.Init(str)
	n, _ = ast.Comment(p)
	n.Print()
	// Output:
	// ["Comment", "Comment 👌 here."]
}
