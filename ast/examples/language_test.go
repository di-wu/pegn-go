package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleLanguage() {
	var n *pegn.Node

	// Language <- Lang ("-" LangExt)?
	p := new(pegn.Parser)

	// PEGN
	p.Init("PEGN")
	n, _ = ast.Language(p)
	n.Print()

	// PEGN-alpa
	p.Init("PEGN-alpha")
	n, _ = ast.Language(p)
	n.Print()

	// Output:
	// ["Language", [
	//   ["Lang", "PEGN"]
	// ]]
	// ["Language", [
	//   ["Lang", "PEGN"],
	//   ["LangExt", "alpha"]
	// ]]

}
