package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleHome() {

	var n *pegn.Node

	// Home <-- (!ws any)+
	p := new(pegn.Parser)

	// gitlab.com/pegn/spec
	p.Init("gitlab.com/pegn/spec")
	n, _ = ast.Home(p)
	n.Print()

	// Output:
	// ["Home", "gitlab.com/pegn/spec"]

}
