package ast_test

import (
	"fmt"

	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleTokenVal_rune() {

	var n *pegn.Node
	var err error

	p := new(pegn.Parser)

	p.Init("x20")
	n, err = ast.TokenVal(p)
	n.Print()
	fmt.Println(err)

	p.Init("\"ABC\"")
	n, _ = ast.TokenVal(p)
	n.Print()

	p.Init("u000F")
	n, _ = ast.TokenVal(p)
	n.Print()

	p.Init("b10")
	n, _ = ast.TokenVal(p)
	n.Print()

	p.Init("x20")
	n, _ = ast.Hexadec(p)
	n.Print()

	p.Init("o744")
	n, _ = ast.Octal(p)
	n.Print()

	// Output:
	// ["TokenVal", [
	//   ["Hexadec", "x20"]
	// ]]
	// <nil>
	// <nil>
	// ["TokenVal", [
	//   ["Unicode", "u000F"]
	// ]]
	// ["TokenVal", [
	//   ["Binary", "b10"]
	// ]]
	// ["Hexadec", "x20"]
	// ["Octal", "o744"]

}

func ExampleTokenVal_string() {

	var n *pegn.Node
	var err error

	p := new(pegn.Parser)

	p.Init("'=>'")
	n, err = ast.TokenVal(p)
	n.Print()
	fmt.Println(err)

	// Output:
	// ["TokenVal", [
	//   ["String", "=>"]
	// ]]
	// <nil>

}
