package ast_test

import (
	"fmt"

	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleResTokenId() {

	var n *pegn.Node

	// ResTokenId <-- 'TAB' / 'LFAT' / 'CR' / 'CRLF' / 'SP' / 'VT' / 'FF' / 'NOT'
	//              / 'BANG' / 'DQ' / 'HASH' / 'DOLLAR' / 'PERCENT' / 'AND'
	//              / 'SQ' / 'LPAREN' / 'RPAREN' / 'STAR' / 'PLUS' / 'COMMA'
	//              / 'DASH' / 'MINUS' /'DOT' /'SLASH' /'COLON' /'SEMI' /'LT'
	//              / 'EQ' / 'GT' / 'QUERY' / 'QUESTION' / 'AT' / 'LBRAKT'
	//              / 'BKSLASH' / 'RBRAKT' / 'CARET' / 'UNDER' / 'BKTICK'
	//              / 'LCURLY' / 'LBRACE' / 'BAR' / 'PIPE' / 'RCURLY'
	//              / 'RBRACE' / 'TILDE' / 'UNKNOWN' / 'REPLACE' / 'MAXRUNE'
	//              / 'MAXASCII' / 'MAXLATIN' / 'LARROWF' / 'RARROWF' / 'LLARROW'
	//              / 'RLARROW' / 'LARROW' / 'LF' / 'RARROW' / 'RFAT'
	//              / 'WALRUS'
	p := new(pegn.Parser)
	var err error

	p.Init("LF")
	n, _ = ast.ResTokenId(p)
	n.Print()

	p.Init("CR")
	n, _ = ast.ResTokenId(p)
	n.Print()

	p.Init("LARROWF LARROW")
	n, _ = ast.ResTokenId(p)
	n.Print()
	_, err = p.Expect(' ')
	fmt.Println(err)
	n, _ = ast.ResTokenId(p)
	n.Print()

	// Output:
	// ["ResTokenId", "LF"]
	// ["ResTokenId", "CR"]
	// ["ResTokenId", "LARROWF"]
	// <nil>
	// ["ResTokenId", "LARROW"]

}
