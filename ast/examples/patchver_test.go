package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExamplePatchVer() {

	var n *pegn.Node

	// PatchVer <-- digit+
	p := new(pegn.Parser)

	// 1
	p.Init("1")
	n, _ = ast.PatchVer(p)
	n.Print()

	// Output:
	// ["PatchVer", "1"]

}
