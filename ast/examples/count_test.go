package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleCount() {
	var n *pegn.Node

	// Count <-- "{" digit+ "}"
	p := new(pegn.Parser)

	// {1}
	p.Init("{1}")
	n, _ = ast.Count(p)
	n.Print()

	// {99}
	p.Init("{99}")
	n, _ = ast.Count(p)
	n.Print()

	// Output:
	// ["Count", "1"]
	// ["Count", "99"]

}
