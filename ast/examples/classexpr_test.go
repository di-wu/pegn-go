package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleClassExpr_ranges() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("[A-Z] / [a-z]")
	n, _ = ast.ClassExpr(p)
	n.Print()
	// Output:
	// ["ClassExpr", [
	//   ["AlphaRange", [
	//     ["Letter", "A"],
	//     ["Letter", "Z"]
	//   ]],
	//   ["AlphaRange", [
	//     ["Letter", "a"],
	//     ["Letter", "z"]
	//   ]]
	// ]]
}

func ExampleClassExpr_classes() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("alphanum / punct")
	n, _ = ast.ClassExpr(p)
	n.Print()
	// Output:
	// ["ClassExpr", [
	//   ["ClassId", [
	//     ["ResClassId", "alphanum"]
	//   ]],
	//   ["ClassId", [
	//     ["ResClassId", "punct"]
	//   ]]
	// ]]
}

func ExampleClassExpr_tokens() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("SP / TAB / CR / LF")
	n, _ = ast.ClassExpr(p)
	n.Print()
	// Output:
	// ["ClassExpr", [
	//   ["TokenId", [
	//     ["ResTokenId", "SP"]
	//   ]],
	//   ["TokenId", [
	//     ["ResTokenId", "TAB"]
	//   ]],
	//   ["TokenId", [
	//     ["ResTokenId", "CR"]
	//   ]],
	//   ["TokenId", [
	//     ["ResTokenId", "LF"]
	//   ]]
	// ]]
}
