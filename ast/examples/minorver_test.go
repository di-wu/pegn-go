package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMinorVer() {
	var n *pegn.Node

	// MinorVer <-- digit+
	p := new(pegn.Parser)

	// 31
	p.Init("31")
	n, _ = ast.MinorVer(p)
	n.Print()

	// Output:
	// ["MinorVer", "31"]

}
