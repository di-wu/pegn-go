package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleOptional() {
	var n *pegn.Node

	// Optional <-- "?"
	p := new(pegn.Parser)

	// ?
	p.Init("?")
	n, _ = ast.Optional(p)
	n.Print()

	// Output:
	// ["Optional", "?"]

}
