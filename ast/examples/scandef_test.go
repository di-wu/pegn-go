package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleScanDef_single() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Rule <- Another")
	n, _ = ast.ScanDef(p)
	n.Print()
	// Output:
	// ["ScanDef", [
	//   ["CheckId", "Rule"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Another"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExampleScanDef_multi() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Simple <- ResClassId / ClassId / Etc")
	n, _ = ast.ScanDef(p)
	n.Print()
	// Output:
	// ["ScanDef", [
	//   ["CheckId", "Simple"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "ResClassId"]
	//       ]]
	//     ]],
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "ClassId"]
	//       ]]
	//     ]],
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Etc"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}
