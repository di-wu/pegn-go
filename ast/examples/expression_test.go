package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleExpression_multiple() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Rule / Another")
	n, _ = ast.Expression(p)
	n.Print()
	// Output:
	// ["Expression", [
	//   ["Sequence", [
	//     ["Plain", [
	//       ["CheckId", "Rule"]
	//     ]]
	//   ]],
	//   ["Sequence", [
	//     ["Plain", [
	//       ["CheckId", "Another"]
	//     ]]
	//   ]]
	// ]]
}

func ExampleExpression_seq() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Here &'are' rules*")
	n, _ = ast.Expression(p)
	n.Print()
	// Output:
	// ["Expression", [
	//   ["Sequence", [
	//     ["Plain", [
	//       ["CheckId", "Here"]
	//     ]],
	//     ["PosLook", [
	//       ["String", "are"]
	//     ]],
	//     ["Plain", [
	//       ["ClassId", "rules"],
	//       ["MinZero", "*"]
	//     ]]
	//   ]]
	// ]]
}
