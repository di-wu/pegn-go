package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExamplePreVer() {
	var n *pegn.Node

	// PreVer <-- PreId ('.' PreId)*
	p := new(pegn.Parser)

	p.Init("alpha")
	n, _ = ast.PreVer(p)
	n.Print()

	p.Init("alpha.1")
	n, _ = ast.PreVer(p)
	n.Print()

	p.Init("0.3.7")
	n, _ = ast.PreVer(p)
	n.Print()

	p.Init("x.7.z.92")
	n, _ = ast.PreVer(p)
	n.Print()

	p.Init("x-y-z.–.")
	n, _ = ast.PreVer(p)
	n.Print()

	// Output:
	// ["PreVer", "alpha"]
	// ["PreVer", "alpha.1"]
	// ["PreVer", "0.3.7"]
	// ["PreVer", "x.7.z.92"]
	// ["PreVer", "x-y-z"]
}
