package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleOctal() {
	var n *pegn.Node

	// Octal <-- "o" octdig+
	p := new(pegn.Parser)

	// o7
	p.Init("o7")
	n, _ = ast.Octal(p)
	n.Print()

	// o007
	p.Init("o007")
	n, _ = ast.Octal(p)
	n.Print()

	// Output:
	// ["Octal", "o7"]
	// ["Octal", "o007"]

}
