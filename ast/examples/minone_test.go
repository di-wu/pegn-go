package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMinOne() {
	var n *pegn.Node

	// MinOne <-- "+"
	p := new(pegn.Parser)

	// +
	p.Init("+")
	n, _ = ast.MinOne(p)
	n.Print()

	// Output:
	// ["MinOne", "+"]

}
