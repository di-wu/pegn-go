package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleSpacing() {
	var n *pegn.Node

	// Spacing <- ComEndLine? SP+
	p := new(pegn.Parser)

	p.Init(" ")
	n, _ = ast.Spacing(p)
	n.Print()

	p.Init("   # Comment\n ")
	n, _ = ast.Spacing(p)
	n.Print()

	// Output:
	// ["Spacing", ""]
	// ["Spacing", [
	//   ["Comment", "Comment"],
	//   ["EndLine", "\n"]
	// ]]

}
