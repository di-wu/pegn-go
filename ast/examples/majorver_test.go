package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMajorVer() {
	var n *pegn.Node

	// MajorVer <-- digit+
	p := new(pegn.Parser)

	// 0
	p.Init("0")
	n, _ = ast.MajorVer(p)
	n.Print()

	// Output:
	// ["MajorVer", "0"]

}
