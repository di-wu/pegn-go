package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// TokenVal <- Rune / SQ String SQ
func TokenVal(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.TokenVal, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// Unicode
	n, err = Unicode(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Binary
	n, err = Binary(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Hexadec
	n, err = Hexadec(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Octal
	n, err = Octal(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	for {

		b := p.Mark()

		_, err = p.Expect('\'')
		if err != nil {
			p.Goto(b)
			break
		}

		n, err = String(p)
		if err != nil {
			p.Goto(b)
			break
		}

		_, err = p.Expect('\'')
		if err != nil {
			p.Goto(b)
			break
		}

		node.AppendChild(n)
		return node, nil
	}

	return expected("TokenVal", node, p)
}
