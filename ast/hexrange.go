package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// HexRange <-- '[' Hexadec '-' Hexadec ']'
func HexRange(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.HexRange, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	_, err = p.Expect('[')
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	// Hexadec
	n, err = Hexadec(p)
	if err != nil {
		p.Goto(beg)
		return expected("Hexadec", node, p)
	}
	node.AppendChild(n)

	// '-'
	_, err = p.Expect('-')
	if err != nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Hexadec
	n, err = Hexadec(p)
	if n == nil {
		p.Goto(beg)
		return expected("Hexadec", node, p)
	}
	node.AppendChild(n)

	// ']'
	_, err = p.Expect(']')
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
