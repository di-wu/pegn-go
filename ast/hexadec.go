package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Hexadec <-- 'x' upperhex+
func Hexadec(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Hexadec, nd.NodeTypes)

	var m *pegn.Mark

	// 'x' upperhex+
	m, _ = p.Check("x", is.Min{is.Upperhex, 1})
	if m == nil {
		return expected("'x' upperhex+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
