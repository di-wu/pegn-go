package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Binary <-- "b" bitdig+
func Binary(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Binary, nd.NodeTypes)

	var m *pegn.Mark
	var err error

	// "b" bin+
	m, err = p.Check("b", is.Min{is.Bitdig, 1})
	if err != nil {
		return expected("'b' [0-1]+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
