package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Grammar <-- Meta? Copyright? Licensed?
//             ComEndLine* (Definition ComEndLine*)+
func Grammar(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Grammar, nd.NodeTypes)

	var err error
	var count int
	var n *pegn.Node

	// Meta?
	n, err = Meta(p)
	if err == nil {
		node.AppendChild(n)
	}

	// Copyright?
	n, err = Copyright(p)
	if err == nil {
		node.AppendChild(n)
	}

	// Licensed?
	n, err = Licensed(p)
	if err == nil {
		node.AppendChild(n)
	}

	// ComEndLine*
	for {
		n, err = ComEndLine(p)
		if err != nil {
			break
		}
		node.AdoptFrom(n)
	}

	// (Definition ComEndLine*)+
	count = 0
	for {

		// Definition
		n, err = Definition(p)
		if err != nil {
			break
		}
		node.AdoptFrom(n)

		// ComEndLine*
		for {
			n, err = ComEndLine(p)
			if err != nil {
				break
			}
			node.AdoptFrom(n)
		}

		count++
	}

	if count == 0 {
		return nil, err
	}

	return node, nil
}
