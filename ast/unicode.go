package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Unicode <-- 'U+' hexdig{4,8}
func Unicode(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Unicode, nd.NodeTypes)

	var m *pegn.Mark

	// 'u' (uphex{4,5} / '10' uphex{4})
	m, _ = p.Check("u", is.OneOf{is.MinMax{is.Upperhex, 4, 5},
		is.Seq{"10", is.Count{is.Upperhex, 4}}},
	)
	if m == nil {
		return expected("'u' (uphex{4,5} / '10' uphex{4})", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
