package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Simple <- Rune / ClassId / TokenId / Range / SQ String SQ
func Simple(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Simple, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// Unicode
	n, err = Unicode(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Binary
	n, err = Binary(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Hexadec
	n, err = Hexadec(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Octal
	n, err = Octal(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// ClassId
	n, err = ClassId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// TokenId
	n, err = TokenId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Range
	n, err = Range(p)
	if err == nil {
		node.AdoptFrom(n)
		return node, nil
	}

	b := p.Mark()

	// SQ
	_, err = p.Expect('\'')
	if err != nil {
		p.Goto(b)
		return expected("SQ", node, p)
	}

	// String
	n, err = String(p)
	if err != nil {
		p.Goto(b)
		return expected("String", node, p)
	}

	// SQ
	_, err = p.Expect('\'')
	if err != nil {
		p.Goto(b)
		return expected("SQ", node, p)
	}

	node.AppendChild(n)
	return node, nil
}
