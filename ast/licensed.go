package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Licensed <-- '# Licensed under ' Comment EndLine
func Licensed(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Licensed, nd.NodeTypes)

	var err error
	var n *pegn.Node
	b := p.Mark()

	// '# Licensed under '
	_, err = p.Expect("# Licensed under ")
	if err != nil {
		p.Goto(b)
		return expected("'# Licensed under '", node, p)
	}

	// Comment
	n, err = Comment(p)
	if err != nil {
		p.Goto(b)
		return expected("Comment", node, p)
	}
	node.AppendChild(n)

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		p.Goto(b)
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
